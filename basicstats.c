// Arthor: Hua Xu
// SP3-lab
// date 11/24/2021

#include <ctype.h>

#include <stdio.h>
#include <string.h>
#include <stdlib.h>

#include <math.h>
#include <stdlib.h>







// read the float, if success, return 0, otherwise return -1;
int read_float(float *f, FILE* data)
{
    int ret = fscanf(data, "%f", f);
    if (ret != 1) {
      return -1;
    }
    return 0;
}


// open the file 
int oepn_file(char *fileName, FILE **fp){
    
    *fp = fopen(fileName,"r");
    if(*fp == NULL){
        return -1;
    }
    else {
        return 0;
    }
}

// close the file
void close_file(FILE **in_file){
    fclose(*in_file);
}



// the swap function to swap two float in array
void swap(float* xp, float* yp)
{
    float temp = *xp;
    *xp = *yp;
    *yp = temp;
}
 
// Function to perform Selection Sort
void selectionSort(float* arr, int index)
{
    int i, j, min_idx;
 
    // One by one move boundary of unsorted subarray
    for (i = 0; i < index - 1; i++) {
 
        // Find the minimum element in unsorted array
        min_idx = i;
        for (j = i + 1; j < index; j++) {
            if (arr[j] < arr[min_idx])
                min_idx = j;
        }
        // Swap the found minimum element
        // with the first element
        swap(&arr[min_idx], &arr[i]);
    }
}

// the funcion to input the file to FILE stream
FILE *inputTheFile(char* str) {

    // Get the number of elements for the array

    FILE *in_file;
    int file_status;
    file_status = oepn_file(str, &in_file);
    // file can not be opened
    if (file_status == -1) {
        printf("can not open the file");
        return NULL;
    }
    return in_file;
}







// expand the array size with 2 times and store the old data into new array
float *expand_array(float *arr, unsigned int num)
{

int c = num*2;
float *ex_arr = (float*)malloc(c*sizeof(float));
if(ex_arr != NULL) // memory allocation is successful
{

// loop to copy integers from arr to ex_arr
for(int i=0;i<num;i++){
ex_arr[i] = arr[i];}
free(arr); // free the allocated memory to arr

arr = ex_arr; // set arr to point to ex_arr
}

return arr; // return the expanded array
}
 
// get the mean of the array and return double
double mean(float *ptr, int index) {
    double total = 0;
    for(int i = 0; i < index; i++) {
        total = total + ptr[i];
    }
    double result = total / index;
    return result;
}
 

//  get the median from the array and return a double
double findMedian(float *ptr, int index)
{
    // First we sort the array
    selectionSort(ptr, index);
 
    // check for even case
    if (index % 2 != 0){
        return (double)ptr[index / 2];
    }
    return (double)(ptr[(index - 1) / 2] + ptr[index / 2]) / 2.0;
}

// find the standard deviation from the array and return a double
double findTheStd(float *ptr, int index, double mean) {
    double total = 0;
     for(int i = 0; i < index; i++) {
        total = total + (ptr[i] - mean)* (ptr[i] - mean) ;
    }
    double stv  = sqrt(total/index);
    return stv;
}

// this is the function for output all stastical result with different colors.
void command(float *ptr, int index, int n) {
    double mean1 = mean(ptr, index);
        double median1 = findMedian(ptr, index);
         double std = findTheStd(ptr,index, mean1);
        printf("\033[0;32m");
        printf("Result: \n");
        printf("\033[0m");
        printf("-------\n");
        printf("\033[0;32m");
        printf("Num ");
        printf("\033[0m");
        printf("values:");
         printf("\033[0;31m");
         printf( "\t%d\n", index);
         printf("\033[0m");
          
         printf("      mean:");
         printf("\033[0;31m");
         printf( "\t%.3f\n", mean1);
         printf("\033[0m");
    
    
        printf("    median:");
         printf("\033[0;31m");
         printf( "\t%.3f\n", median1);
         printf("\033[0m");
    
    
         printf("    stddev:");
         printf("\033[0;31m");
         printf( "\t%.3f\n", std);
         printf("\033[0m");
    
    
       printf("\033[0;32m");
       printf("Unused ");
       printf("\033[0m");
        printf("array capacity");
    
         printf("\033[0;31m");
         printf( ": %d\n", n-index);
         printf("\033[0m");
}
 
int main(int argc,char* argv[])
{
    // the initial array size is 20.
    int n = 20;
    // create the dynamic array
    FILE* in_file = inputTheFile(argv[1]);
    int index = 0;
    // create the pointer with size = 20,
    float* ptr = (float*)malloc(n * sizeof(float));
     int ret = 0;   
        while(read_float(&ptr[index],in_file) != -1) {
            index++;
            if(index == n) {
              ptr = expand_array(ptr, n);
              n = n * 2;
            }  
        }       
        command(ptr, index, n);
  fclose(in_file);
     // free the memory of ptr
       free(ptr);

    return 0;
}